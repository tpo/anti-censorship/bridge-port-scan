# obfs4PortScan

This service lets bridge operators test if their bridge's obfs4 port is
publicly reachable.

## Command line arguments arguments

The tool takes a number of command line arguments:

- `-cert-file`: a path to a certificate file to run the HTTPS server
- `-key-file`: a path to a key file to run the HTTPS server
- `-addr`: set the address and port to listen on (defaults to `:4000`)
- `-templatedir`: set the template directory (defaults to `/home/bridgescan/bridge-port-scan/`)

## Scanning method

We try to establish a TCP connection with the given IP address and port using
golang's `net.DialTimeout` function.  If we don't get a response within three
seconds, we deem the port unreachable.  We also deem the port unreachable if we
get a RST segment before the timeout.  In both cases, we display the error
message that we got from `net.DialTimeout`.

We implement a simple rate limiter that limits incoming requests to an average
of one per second with bursts of as many as five requests per second.

## Dependencies

PostScan needs go 1.17 or greater to build.

PortScan requires a number of dependencies to build the frontend: python3, pip, gettext, venv, and lektor. These can be installed on debian with `sudo apt install python3 python3-pip gettext python3-venv lektor`

The usual lektor build process causes some issues with PortScan, so this site also requires the Dart SASS compiler. An installation script for Dart SASS can be found [here](https://gitlab.torproject.org/tpo/web/wiki/-/wikis/How-to-edit-css#installing-the-sass-compiler). **NOTE:** The npm implementation of SASS doesn't compile our SASS, so you'll need to use the script linked above.

## Building

After installing the dependencies, PortScan can be built by running the `build.sh` script in the project root. Lektor likes to install plugins globally, so activating a virtualenv beforehand is recommended: `python3 -m venv venv && . venv/bin/activate`

The build script will create a `public` directory in the project root. This directory contains the templates and static files used by PortScan, and should be used as the value for the `-templatedir` argument to PortScan.

## Testing with Docker

The bridges.tpo production environment adds some challenges (CSP, reverse-proxying from `/scan`) that makes the lektor dev server unsuitable for testing. This repo provides a set of Dockerfiles and a docker-compose script. After building bridge-port-scan, run `docker-compose up` from the command line, and the test server will be accessible at <http://127.0.0.1:8080/scan>.

## Deployment

After running the build script, shut down the obfs4PortScan service on BridgeDB which runs under the
bridgescan user:

    systemctl --user stop obfs4portscan.service

Then, copy the binary onto BridgeDB's host. It belongs into the directory `/home/bridgescan/bin/`. Then copy the `public` directory to your `templatedir` (defaults to `/home/bridgescan/bridge-port-scan`). Once everything is in place, restart the service:

    systemctl --user start obfs4portscan.service

## Maintaining

PortScan doesn't require much updating. It's feature complete, and only rarely has a bug that needs to be addressed. The biggest maintenence task it requires is a periodic lego update.

```
git -C frontend/lego fetch && git -C frontend/lego checkout origin/main
```

Make sure the site still builds with the new lego updates, commit, and push.

## License

PortScan is licensed under the BSD-2-Clause license. See the LICENSE file in this repository for more information.
