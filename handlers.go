package main

import (
	"bytes"
	"errors"
	"fmt"
	"golang.org/x/time/rate"
	"net"
	"net/http"
	"strings"
	"time"
)

// timeout specifies the number of seconds we're willing to wait until we
// decide that the given destination is offline.
const timeout time.Duration = 3 * time.Second

// limiter implements a rate limiter.  We allow 1 request per second on average
// with bursts of up to 5 requests per second.
var limiter = rate.NewLimiter(1, 5)

// Index returns the landing page template
// (which contains the scanning form) to the user.
func Index(w http.ResponseWriter, r *http.Request) {

	buf := new(bytes.Buffer)
	tpl := templates["index.html"]
	tpl.Execute(buf, struct{}{})
	SendResponse(w, buf.String())
}

// SendResponse sends the given response to the user.
func SendResponse(w http.ResponseWriter, response string) {

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintln(w, response)
}

// SendError returns an HTTP 500 error if reading a template goes wrong.
func SendError(w http.ResponseWriter) {

	// TODO: Log an error here
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintln(w, "<h1>Internal Server Error</h1>")
}

// ScanDestination extracts the given IP address and port from the GET parameters,
// triggers TCP scanning of the tuple, and returns the result.
func ScanDestination(w http.ResponseWriter, r *http.Request) {

	// We implement rate limiting to prevent someone from abusing this service
	// as a port scanner.
	if limiter.Allow() == false {
		SendResponse(w, "Rate limit exceeded.  Wait for a bit.")
		return
	}

	r.ParseForm()
	// These variables will be "" if they're not set.
	address := r.Form.Get("address")
	port := r.Form.Get("port")

	if address == "" {
		SendResponse(w, FailurePage(errors.New("No address given.")))
		return
	}
	if port == "" {
		SendResponse(w, FailurePage(errors.New("No port given.")))
		return
	}

	var parsed_ip net.IP = nil

	if strings.HasPrefix(address, "[") && strings.HasSuffix(address, "]") {
		parsed_ip = net.ParseIP(address[1 : len(address)-1])
	} else {
		parsed_ip = net.ParseIP(address)
	}

	if parsed_ip == nil {
		SendResponse(w, FailurePage(errors.New("Invalid IP address: "+address)))
		return
	}

	if parsed_ip.IsPrivate() || parsed_ip.IsLoopback() || parsed_ip.IsLinkLocalMulticast() || parsed_ip.IsLinkLocalUnicast() {
		SendResponse(w, FailurePage(errors.New("IP address "+address+" is private. If you're trying to scan an address on your local network, you'll need to scan the public IP of your bridge.")))
		return
	}

	portReachable, err := IsTCPPortReachable(address, port)
	if portReachable {
		buf := new(bytes.Buffer)
		tpl := templates["success.html"]
		tpl.Execute(buf, struct{}{})
		SendResponse(w, buf.String())
	} else {
		SendResponse(w, FailurePage(err))
	}
}

// IsTCPPortReachable returns `true' if it can establish a TCP connection with
// the given IP address and port.  If not, it returns `false' and the
// respective error, as reported by `net.DialTimeout'.
func IsTCPPortReachable(addr, port string) (bool, error) {

	conn, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%s", addr, port), timeout)
	if err != nil {
		return false, err
	}
	conn.Close()
	return true, nil
}

// FailurePage is shown when the given address and port are unreachable.
func FailurePage(reason error) string {

	buf := new(bytes.Buffer)
	tpl := templates["failure.html"]
	tpl.Execute(buf, reason)
	return buf.String()
}
