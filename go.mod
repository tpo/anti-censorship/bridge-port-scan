module gitlab.torproject.org/tpo/anti-censorship/bridge-port-scan

go 1.17

require (
	git.torproject.org/pluggable-transports/snowflake.git v1.1.0
	github.com/gorilla/mux v1.8.0
	golang.org/x/time v0.0.0-20211116232009-f0f3c7e86c11
)
