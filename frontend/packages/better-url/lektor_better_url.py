# -*- coding: utf-8 -*-
from lektor.pluginsystem import Plugin
from urllib.parse import urlparse, urlunparse
import os

class BetterUrlPlugin(Plugin):
    name = 'better_url'
    description = u'Add your description here.'

    def on_setup_env(self, **extra):
        def better_url(url, *args, **kwargs):
            parsed_base_url = urlparse(self.get_lektor_config().base_url)._replace(scheme='')._replace(netloc='')
            if url[0] == '/':
                url = url[1:]
            return os.path.join(urlunparse(parsed_base_url), url)

        self.env.jinja_env.filters.update(better_url=better_url, url=better_url, asseturl=better_url)
