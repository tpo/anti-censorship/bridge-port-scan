package main

import (
	"flag"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"time"

	"git.torproject.org/pluggable-transports/snowflake.git/common/safelog"
	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

// NewRouter creates and returns a new request router.
func NewRouter(frontendDir string, routes Routes) *mux.Router {

	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler

		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)

		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}

	var staticDir = path.Join(frontendDir, "static")

	router.PathPrefix("/static/").Handler(
		http.StripPrefix("/static/", http.FileServer(http.Dir(staticDir))))

	return router
}

// Logger logs when we receive requests, and the execution time of handling
// these requests.  We don't log client IP addresses or the given obfs4
// parameters.
func Logger(inner http.Handler, name string) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		inner.ServeHTTP(w, r)

		log.Printf(
			"%s\t%s\t%s\t%s",
			r.Method,
			r.RequestURI,
			name,
			time.Since(start),
		)
	})
}

//var templates = make(map[string]string)
var templates = make(map[string]*template.Template)

// main is the entry point of this tool.
func main() {

	var addr string
	var frontendDir string

	flag.StringVar(&addr, "addr", ":4000", "Address to listen on.")
	flag.StringVar(&frontendDir, "templatedir", "/home/bridgescan/bridge-port-scan/",
		"Directory containing the bridge-port-scan templates")
	flag.Parse()

	templateDir := path.Join(frontendDir, "templates")

	err := LoadTemplates(templateDir, templates, templateDir)
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	var routes = Routes{
		Route{
			"Index",
			"GET",
			"/",
			Index,
		},
		Route{
			"ScanDestination",
			"GET",
			"/scan",
			ScanDestination,
		},
	}

	var logOutput io.Writer = os.Stderr
	// We want to send the log output through our scrubber first
	log.SetOutput(&safelog.LogScrubber{Output: logOutput})
	log.SetFlags(log.LstdFlags | log.LUTC)

	router := NewRouter(frontendDir, routes)
	log.Printf("Now listening on %s", addr)
	log.Fatal(http.ListenAndServe(addr, router))
}
