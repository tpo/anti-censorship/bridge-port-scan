#!/usr/bin/env bash

set -ex

git submodule update --init

rm -rf public

pushd frontend
rm -rf public
mkdir -p public/templates

cp scan.scss assets/scss/_scan.scss

if grep -i '@import "scan";' assets/scss/bootstrap.scss; then
    echo '@import "scan";' >> assets/scss/bootstrap.scss
fi

pip install -r lego/lektor-requirements.txt
pip install -r requirements.txt

sass assets/scss:assets/static/css

rm -rf lektoroutput
LEKTOR_ENV=dev lektor b -O lektoroutput

mv lektoroutput/index.html public/templates
mv lektoroutput/failure/index.html public/templates/failure.html
mv lektoroutput/success/index.html public/templates/success.html

mv lektoroutput/static public

mv public ..

popd
go build
